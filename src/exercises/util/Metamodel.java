package exercises.util;

import exercises.annotation.Column;
import exercises.annotation.PrimaryKey;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Metamodel<T> {
    private Class<T> clss;

    public static <T> Metamodel of(Class<T> clss) {
        return new Metamodel<>(clss);
    }

    public Metamodel(Class<T> clss) {
        this.clss = clss;
    }


    public PrimaryKeyField getPrimaryKey() {
        Field[] fields = clss.getDeclaredFields();
        for(Field field : fields){
            PrimaryKey primaryKey = field.getAnnotation(PrimaryKey.class);
            if(primaryKey != null){
                PrimaryKeyField primaryKeyField = new PrimaryKeyField(field);
                return primaryKeyField;
            }
        }
        throw new IllegalArgumentException("No primary key found in class " + clss.getName());
    }

    public List<ColumnField> getColumn() {
        List<ColumnField> columnFields  = new ArrayList<>();
        Field[] fields = clss.getDeclaredFields();
        for(Field field : fields){
            Column column = field.getAnnotation(Column.class);
            if(column != null ){
                ColumnField columnField = new ColumnField(field);
                columnFields.add(columnField);
            }
        }
        return columnFields;

    }

    public String buildInsertRequest() {
        //insert into person (id,name, value) values (?,?,?)
        String columnElement = buildColumnNames();
        String questionMaskElement = buildQuestionMaskElement();

        return "insert into " + this.clss.getSimpleName() +
                "( " + columnElement + ") value (" + questionMaskElement + ") ";
    }

    public String buildSelectRequest() {
        //select id name
        String columnElement = buildColumnNames();
        return "select " + columnElement + "from " + this.clss.getSimpleName() + "where "
                + getPrimaryKey().getName() + "?";
    }

    private String buildQuestionMaskElement() {
        int numberOfColumns = getColumn().size() + 1;
        String questionMaskElement = IntStream.range(0 ,numberOfColumns)
                .mapToObj(index -> "?")
                .collect(Collectors.joining(", "));
        return questionMaskElement;
    }

    private String buildColumnNames() {
        String primaryKeyColumnName = getPrimaryKey().getName();
        List<String> columnNames = getColumn().stream().map(ColumnField::getName).collect(Collectors.toList());
        columnNames.add(0,primaryKeyColumnName);
        String columnElement = String.join(",", columnNames);
        return columnElement;
    }
}
