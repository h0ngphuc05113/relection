package exercises;

import exercises.util.ColumnField;
import exercises.util.Metamodel;
import exercises.util.PrimaryKeyField;

import java.util.List;

public class PlayWithMetamodel {

    public static void main(String...args){
        Metamodel<Person> metamodel = Metamodel.of(Person.class);

        PrimaryKeyField primaryKeyField = metamodel.getPrimaryKey();
        List<ColumnField> columns = metamodel.getColumn();

        System.out.println("Primary key name = " + primaryKeyField.getName() + " Type = " + primaryKeyField.getType().getSimpleName());
        for(ColumnField columnField : columns){
            System.out.println("Column key name = " + columnField.getName() + " Type = " + columnField.getType().getSimpleName());

        }
    }
}
