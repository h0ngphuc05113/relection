package exercises;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException, IOException, NoSuchFieldException {
//        Class<String> stringClass = String.class;

        Map<String, Integer> mapObject = new HashMap<>();
        Class<?> hashMapClass = mapObject.getClass();
        Class<? extends String> questionMark = "QuestionMark".getClass();
        Class<? extends Object> objectQuestionMark = "QuestionMark".getClass(); // error : Class<String> questionMark = "QuestionMark".getClass();
        //forName
        Class<?> stringClass = String.class;
        String className = "java.lang.String";
        Class<?> stringClassOne = Class.forName(className);
        Class<?> stringClassTwo = Class.forName("java.lang.String");
        //getting the super class
        Class<?> classSuper = "Hello".getClass();
        Class<?> superClass = classSuper.getSuperclass();
        Class<?>[] interfaceSuperClass = classSuper.getInterfaces();
        //getting the field of class
        Class<?> personClass = Person.class;
//        Field field = personClass.getField("age");
        Field[] declaredFields = personClass.getDeclaredFields();
        Field[] fields = personClass.getFields();
        //getting the methods and
        /*
        getMethod(name,types)
        getDeclaredMethod(): declared in the class
        getMethods(): public methods, including inherited
         */
        Class<?> classMethod = Person.class;
//        Method method = classMethod.getMethod("setName", String.class);
//        Constructor contructor = classMethod.getConstructor(Class<?>... types);
        Constructor[] declaredConstructors = classMethod.getDeclaredConstructors();
        Constructor[] constructors = classMethod.getConstructors();
        /*
        Reading the modifier to tell
        use getModifiers() return an integet on 32 bits
         */
//        Field modifierPerson = classMethod.getField("name");
//        int modifiers = modifierPerson.getModifiers();
//        boolean isPublic = modifiers && 0x00000001;
//        boolean isPublicOne = Modifier.isPublic(modifiers);


        Class<?> squareClass = Class.forName("exercises.Main$Square");
//        printClassInfo(stringClass, hashMapClass, squareClass);
//        var circleObject = new Drawable() {
//            @Override
//            public int getNumberPOfCorners() {
//                return 0;
//            }
//        };
//
//        printClassInfo(Collections.class, boolean.class, int[][].class, Color.class, circleObject.getClass());

        /// function reflection
//        initConfiguration();
//        WebServer webServer = new WebServer();
//        webServer.startServer();


        //Demo

        String personClassName = "exercises.Person";
        Class<?> personClassDemo = Class.forName(personClassName);
        System.out.println("Person class = " + personClassDemo);

        System.out.println("====================");
        Field[] fieldsList = personClassDemo.getFields();
        System.out.println("Field list: " + fieldsList);
        System.out.println("Field list One: " + Arrays.toString(fieldsList));

        System.out.println("====================");
        Field[] declaredFieldsList = personClassDemo.getDeclaredFields();
        System.out.println("DeclaredField list: ");
        System.out.println(Arrays.toString(declaredFieldsList));


        System.out.println("==================== Method");
        Method[] methods = personClassDemo.getMethods();
        for (Method method : methods) {
            System.out.println(method);
        }

        System.out.println("====================Declared Method");
        Method[] declaredMethods = personClassDemo.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            System.out.println(declaredMethod);
        }

        System.out.println("====================Static Declared Method");
        Arrays.stream(declaredMethods).filter(m -> Modifier.isStatic(m.getModifiers())).forEach(System.out::println);

    }

    public static void initConfiguration() throws InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        Constructor<ServerConfiguration> constructor =
                ServerConfiguration.class.getDeclaredConstructor(int.class, String.class);
        constructor.setAccessible(true);
        constructor.newInstance(8080, "Good boy!");
    }

    static void printClassInfo(Class<?>... classes) {

        for (Class<?> clazz : classes) {
            System.out.println(String.format("class name: %s , class package name : %s", clazz.getSimpleName(),
                    clazz.getPackageName()));

            Class<?>[] implementedInterfaces = clazz.getInterfaces();


            for (Class<?> implementInterfaces : implementedInterfaces) {
                System.out.println(String.format("class %s implement : %s",
                        clazz.getSimpleName(),
                        implementInterfaces.getSimpleName()));
            }
            System.out.println("is Interface : " + clazz.isInterface());
            System.out.println("is array:" + clazz.isArray());
            System.out.println("is primitive:" + clazz.isPrimitive());
            System.out.println("is enum:" + clazz.isEnum());
            System.out.println("is anonumous :" + clazz.isAnonymousClass());
            System.out.println("is anonumous :" + clazz.isAnnotation());
            System.out.println();
            System.out.println("");
            System.out.println("");
        }
    }

    static class Square implements Drawable {

        @Override
        public int getNumberPOfCorners() {
            return 4;
        }
    }

    private static interface Drawable {
        int getNumberPOfCorners();
    }

    enum Color {
        BLUE,
        RED,
        GREEN
    }
}
