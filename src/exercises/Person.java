package exercises;

import exercises.annotation.Column;
import exercises.annotation.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class Person {
    @PrimaryKey
    private Long id;

    @Column
    private Address address;

    @Column
    private String name;

    @Column
    private int age;

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person(String name, int age, Address address) {
        this.name = name;
        this.age = age;
        this.address = address;
    }

    @Override
    public String toString() {
        return "PersonSub : [" +
                "address = " + address +
                ", name ='" + name + '\'' +
                ", age =" + age +
                ']';
    }

    //    public static void main(String[] args) throws InvocationTargetException, InstantiationException, IllegalAccessException {
////        printConstructorData(Address.class);
//        Address address = createInstanceWithArgument(Address.class, "Ha Noi", 10);
//
//        PersonSub person = createInstanceWithArgument(PersonSub.class, "Phuc", 10);
//        System.out.println(person);
//    }


//    public static <T> T createInstanceWithArgument(Class<T> clazz, Object... args) throws InvocationTargetException, InstantiationException, IllegalAccessException {
//        for (Constructor<?> constructor : clazz.getDeclaredConstructors()) {
//            if (constructor.getParameterTypes().length == args.length) {
//                return (T) constructor.newInstance(args);
//            }
//        }
//        System.out.println("An appropriate constructor was not found ");
//        return null;
//    }

    public static class PersonSub {
        Address address;
        String name;
        int age;


        public PersonSub() {
        }

        public PersonSub(String name) {
            this.name = name;
        }

        public PersonSub(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public PersonSub(String name, int age, Address address) {
            this.name = name;
            this.age = age;
            this.address = address;
        }

        @Override
        public String toString() {
            return "PersonSub : [" +
                    "address = " + address +
                    ", name ='" + name + '\'' +
                    ", age =" + age +
                    ']';
        }
    }


    public static class Address {
        private String street;
        private int number;

        public Address(String street, int number) {
            this.street = street;
            this.number = number;
        }
    }


    public static void printConstructorData(Class<?> clazz) {
        Constructor<?>[] contructors = clazz.getDeclaredConstructors();

        System.out.println(String.format("class %s has %d declared constructors ", clazz.getSimpleName(), contructors.length));
        for (int i = 0; i < contructors.length; i++) {
            Class<?>[] parameterTypes = contructors[i].getParameterTypes();
            List<String> parameterTypeNames = Arrays.stream(parameterTypes).map(type -> type.getSimpleName()).collect(Collectors.toList());

            System.out.println(parameterTypeNames);
        }
    }

}
