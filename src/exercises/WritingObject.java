package exercises;

import exercises.orm.EntityManager;

import java.sql.SQLException;

public class WritingObject {

    public static void main(String[] agrs) throws SQLException, IllegalAccessException {

        EntityManager<Person> entityManager = EntityManager.of(Person.class);
        Person phuc = new Person("Phuc",23);
        Person phong = new Person("Phong",22);
        Person longOne = new Person("Long",21);

        System.out.println(phuc);
        System.out.println(phong);
        System.out.println(longOne);

        System.out.println("Writing to DB");
        entityManager.persist(phuc);
        entityManager.persist(phong);
        entityManager.persist(longOne);


        System.out.println(phuc);
        System.out.println(phong);
        System.out.println(longOne);


    }
}
